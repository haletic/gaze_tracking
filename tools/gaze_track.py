from pynput import mouse
import cv2
import os
from datetime import datetime

# try different ids to capture from correct webcam
CAMERA_ID = 0

class Gazetracking:
    def __init__(self):
        self.frame_id = 0
        self.dir = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        os.mkdir(self.dir)


    def on_click(self, x, y, button, pressed):
        if pressed:
            print(f'Pointer pressed at {x}, {y}')
            camera = cv2.VideoCapture(CAMERA_ID)
            ret, image = camera.read()
            if not ret:
                raise RuntimeError("ret is False")
            cv2.imwrite(f'{self.dir}/frame_{self.frame_id}.png', image)
            self.frame_id = self.frame_id + 1
            camera.release()

    def start_capture(self):
        with mouse.Listener(
                on_click=self.on_click) as listener:
            listener.join()

gaze_tracker = Gazetracking()
gaze_tracker.start_capture()
